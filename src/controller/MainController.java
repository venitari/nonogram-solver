package controller;

import controller.nonogram.wizard.pages.ColumnDefinitionPageController;
import controller.nonogram.wizard.pages.RowDefinitionPageController;
import controller.nonogram.wizard.pages.SizePageController;
import controller.wizard.WizardController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.definition.TableDefinition;
import model.permutation.FieldBoardPermutation;
import model.user.input.DefinitionFileLoad;
import model.user.input.DefinitionFileSave;
import model.user.input.NonogramDefinitionInput;
import view.DrawingAreaTab;

import java.io.File;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class MainController {
    public MainController() {
        try {
            fileSelectWindow.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text file", "*.txt"));
            initWizard();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void newMenuItemClick(ActionEvent actionEvent) {
        try {
            wizardController.setUserData(new NonogramDefinitionInput());
            wizardController.setFirstPage();
            wizardStage.show();
        } catch (Exception e) {
            Alert errorMessage = new Alert(Alert.AlertType.ERROR, "Wizard opening failed");
            errorMessage.show();
            e.printStackTrace();
        }
    }

    public void loadMenuItemClick(ActionEvent actionEvent) {
        File file = fileSelectWindow.showOpenDialog(rootPane.getScene().getWindow());

        if (file == null)
            return;

        try {
            NonogramDefinitionInput input = DefinitionFileLoad.loadFromFile(file);
            wizardController.setUserData(input);
            wizardController.setFirstPage();
            wizardStage.show();
        } catch (Exception e) {
            Alert errorMessage = new Alert(Alert.AlertType.ERROR, "File opening failed");
            errorMessage.show();
            e.printStackTrace();
        }
    }

    public void saveMenuItemClick(ActionEvent actionEvent) {
        File file = fileSelectWindow.showSaveDialog(rootPane.getScene().getWindow());

        if (file == null)
            return;

        try {
            DefinitionFileSave.saveToFile(file, userInput);
        } catch (Exception e) {
            Alert errorMessage = new Alert(Alert.AlertType.ERROR, "File saving failed");
            errorMessage.show();
            e.printStackTrace();
        }
    }

    private void initWizard() throws Exception {
        FXMLLoader wizardLoader = new FXMLLoader(getClass().getResource("../view/wizard.fxml"));
        Parent root = wizardLoader.load();
        wizardController = wizardLoader.getController();
        wizardController.addWizardPageController(new SizePageController());
        wizardController.addWizardPageController(new ColumnDefinitionPageController());
        wizardController.addWizardPageController(new RowDefinitionPageController());
        wizardController.setPostWizardAction(this::drawNonogram);
        wizardStage.setTitle("Add new image definition");
        wizardStage.setScene(new Scene(root, 300, 300));
    }

    private void drawNonogram(NonogramDefinitionInput inputData) {
        userInput = inputData;
        TableDefinition definition = new TableDefinition(inputData);
        FieldBoardPermutation permutation = new FieldBoardPermutation(definition);
        permutation.reset();

        DrawingAreaTab drawingTab = new DrawingAreaTab("Solution");
        nonogramPrinter.setCanvas(drawingTab.getCanvas());
        nonogramPrinter.printNonogram(permutation.getCurrentBoard());
        tabs.getTabs().add(drawingTab);
    }

    @FXML
    private BorderPane rootPane;
    @FXML
    private TabPane tabs;
    private FileChooser fileSelectWindow = new FileChooser();
    private Stage wizardStage = new Stage();
    private WizardController<NonogramDefinitionInput> wizardController;
    private NonogramDefinitionInput userInput;
    private CanvasNonogramPrinter nonogramPrinter = new CanvasNonogramPrinter();
}
