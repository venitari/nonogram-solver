package controller.wizard;

import javafx.scene.Node;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public abstract class WizardPageController<T> {
    public WizardPageController() {
        initView();
    }

    public abstract void setUserInputData(T userInputData);

    public abstract boolean validateData();

    public abstract String validationMessage();

    public abstract void prepareView();

    public abstract void initView();

    public abstract Node getView();

    public abstract String getDescription();
}
