package controller.wizard;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class WizardController<UserModel> {
    public void setFirstPage() {
        currentPage = 0;
        showPage();
    }

    public void setUserData(UserModel userData) {
        this.userData = userData;
        for (WizardPageController<UserModel> page : pages)
            page.setUserInputData(this.userData);
    }

    public void setPostWizardAction(Consumer<UserModel> postWizardAction) {
        this.postWizardAction = postWizardAction;
    }

    @FXML
    public void nextBtnAction(ActionEvent event) {
        if (!validPage())
            return;

        if (lastPage()) {
            finishWizard();
            return;
        }

        currentPage++;
        showPage();
    }

    public void addWizardPageController(WizardPageController<UserModel> page) {
        page.setUserInputData(userData);
        pages.add(page);
    }

    @FXML
    public void prevBtnAction(ActionEvent event) {
        currentPage--;
        showPage();
    }

    private void finishWizard() {
        Stage stage = (Stage) wizardPage.getScene().getWindow();
        postWizardAction.accept(userData);
        stage.close();
    }

    private boolean lastPage() {
        return currentPage >= pages.size() - 1;
    }

    private boolean firstPage() {
        return currentPage <= 0;
    }

    private boolean validPage() {
        WizardPageController<UserModel> currentController = pages.get(currentPage);
        if (!currentController.validateData()) {
            Alert alert = new Alert(Alert.AlertType.WARNING, currentController.validationMessage());
            alert.show();
            return false;
        }

        return true;
    }

    private void showPage() {
        wizardPage.getChildren().clear();
        WizardPageController<UserModel> currentController = pages.get(currentPage);
        currentController.prepareView();
        BorderPane.setMargin(currentController.getView(), new Insets(0, 0, 0, 0));
        wizardPage.getChildren().addAll(currentController.getView());
        descriptionLabel.setText(currentController.getDescription());
        setButtonsEnability();
    }

    private void setButtonsEnability() {
        if (lastPage())
            nextBtn.setText("Finish");
        else
            nextBtn.setText("Next");

        if (firstPage())
            prevBtn.setDisable(true);
        else
            prevBtn.setDisable(false);
    }

    @FXML
    private VBox wizardPage;
    @FXML
    private Button nextBtn;
    @FXML
    private Button prevBtn;
    @FXML
    private Label descriptionLabel;

    private int currentPage;
    private Consumer<UserModel> postWizardAction;
    private UserModel userData;
    private List<WizardPageController<UserModel>> pages = new ArrayList<>();
}
