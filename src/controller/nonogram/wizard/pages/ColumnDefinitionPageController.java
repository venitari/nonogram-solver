package controller.nonogram.wizard.pages;

import controller.wizard.WizardPageController;
import javafx.scene.Node;
import model.user.input.NonogramDefinitionInput;
import view.wizard.pages.ColumnDefinitionPage;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class ColumnDefinitionPageController extends WizardPageController<NonogramDefinitionInput> {
    @Override
    public void setUserInputData(NonogramDefinitionInput userInputData) {
        inputData = userInputData;
    }

    @Override
    public boolean validateData() {
        inputData.setColumnsSequencesLengths(page.getVectorsSequencesLengths());
        return true;
    }

    @Override
    public String validationMessage() {
        return "mistaik ;_;";
    }

    @Override
    public void prepareView() {
        page = new ColumnDefinitionPage(inputData.getWidth());
        page.fillTable(inputData.getColumnsSequencesLengths());
    }

    @Override
    public void initView() {

    }

    @Override
    public Node getView() {
        return page.getView();
    }

    @Override
    public String getDescription() {
        return "Insert column length sequences:";
    }

    private NonogramDefinitionInput inputData;
    private ColumnDefinitionPage page;
}