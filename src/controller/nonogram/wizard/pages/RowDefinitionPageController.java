package controller.nonogram.wizard.pages;

import controller.wizard.WizardPageController;
import javafx.scene.Node;
import model.user.input.NonogramDefinitionInput;
import view.wizard.pages.RowDefinitionPage;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class RowDefinitionPageController extends WizardPageController<NonogramDefinitionInput> {
    @Override
    public void setUserInputData(NonogramDefinitionInput userInputData) {
        inputData = userInputData;
    }

    @Override
    public boolean validateData() {
        inputData.setRowsSequencesLengths(page.getVectorsSequencesLengths());
        return true;
    }

    @Override
    public String validationMessage() {
        return "MISTAIK ;_;";
    }

    @Override
    public void prepareView() {
        page = new RowDefinitionPage(inputData.getHeight());
        page.fillTable(inputData.getRowsSequencesLengths());
    }

    @Override
    public void initView() {

    }

    @Override
    public Node getView() {
        return page.getView();
    }

    @Override
    public String getDescription() {
        return "Insert row length sequences:";
    }

    private NonogramDefinitionInput inputData;
    private RowDefinitionPage page;
}
