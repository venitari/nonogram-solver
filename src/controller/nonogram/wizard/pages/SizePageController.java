package controller.nonogram.wizard.pages;

import controller.wizard.WizardPageController;
import javafx.scene.Node;
import model.user.input.NonogramDefinitionInput;
import view.wizard.pages.SizePage;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class SizePageController extends WizardPageController<NonogramDefinitionInput> {
    @Override
    public void setUserInputData(NonogramDefinitionInput userInputData) {
        inputData = userInputData;
    }

    @Override
    public boolean validateData() {
        int width;
        int height;

        try {
            width = Integer.parseInt(page.getWidthText());
            height = Integer.parseInt(page.getHeightText());
        } catch (Exception e) {
            errorMessage = "Invalid data format. Values are supposed to be non-negative integer.";
            return false;
        }

        if (width <= 0 || height <= 0) {
            errorMessage = "Values are not non-negative.";
            return false;
        }

        inputData.setWidth(width);
        inputData.setHeight(height);
        return true;
    }

    @Override
    public String validationMessage() {
        return errorMessage;
    }

    @Override
    public void prepareView() {
        if (inputData.getHeight() > 0 && inputData.getWidth() > 0) {
            page.setHeightText(String.valueOf(inputData.getHeight()));
            page.setWidthText(String.valueOf(inputData.getWidth()));
        }
    }

    @Override
    public void initView() {
        page = new SizePage();
    }

    @Override
    public Node getView() {
        return page.getView();
    }

    @Override
    public String getDescription() {
        return "Insert nonogram dimensions:";
    }

    private String errorMessage = "";
    private SizePage page;
    private NonogramDefinitionInput inputData;
}
