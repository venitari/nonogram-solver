package controller;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.FieldBoard;

/**
 * Created by Patryk Dziedzic on 21.03.2017.
 */
public class CanvasNonogramPrinter {
    public void printNonogram(FieldBoard board) {
        if (boardCanvas == null)
            return;

        GraphicsContext gc = prepareDrawingArea(board);
        int x = margin;
        int y = margin;

        for (int ix = 0; ix < board.getWidth(); ix++) {
            y = margin;

            for (int iy = 0; iy < board.getHeight(); iy++) {
                if (board.select(ix, iy).isFilled()) {
                    gc.setFill(fillColor);
                    gc.fillRect(x, y, fieldSize, fieldSize);
                } else if (board.select(ix, iy).isUnknown()) {
                    gc.setFill(unknownColor);
                    gc.fillRect(x, y, fieldSize, fieldSize);
                }

                y += fieldSize;
            }

            x += fieldSize;
        }

        drawBorders(board, gc);
    }

    public void setCanvas(Canvas boardCanvas) {
        this.boardCanvas = boardCanvas;
    }

    private void drawBorders(FieldBoard board, GraphicsContext gc) {
        int x = margin;
        int y = margin;

        int width = board.getWidth() * (fieldSize);
        int height = board.getHeight() * (fieldSize);

        gc.strokeRect(x, y, width, height);

        for (int ix = 1; ix < board.getWidth(); ix++) {
            x += fieldSize;
            gc.strokeLine(x, y, x, y + height);
        }

        x = margin;

        for (int iy = 1; iy < board.getHeight(); iy++) {
            y += fieldSize;
            gc.strokeLine(x, y, x + width, y);
        }
    }

    private GraphicsContext prepareDrawingArea(FieldBoard board) {
        int width = board.getWidth() * (fieldSize) + 2 * margin;
        int height = board.getHeight() * (fieldSize) + 2 * margin;
        boardCanvas.setWidth(width);
        boardCanvas.setHeight(height);
        GraphicsContext gc = boardCanvas.getGraphicsContext2D();
        gc.setFill(backgroundColor);
        gc.fillRect(0, 0, width, height);
        gc.setStroke(lineColor);
        gc.setLineWidth(lineWidth);
        gc.clearRect(0, 0, boardCanvas.getWidth(), boardCanvas.getHeight());
        return gc;
    }

    private Canvas boardCanvas;
    private static Color lineColor = Color.BLACK;
    private static Color fillColor = Color.CRIMSON;
    private static Color unknownColor = Color.BEIGE;
    private static Color backgroundColor = Color.WHITE;
    private static int fieldSize = 30;
    private static int lineWidth = 3;
    private static int margin = 10;
}
