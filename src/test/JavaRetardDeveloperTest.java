package test;

import model.Field;
import model.FieldStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class JavaRetardDeveloperTest {
    @Test
    public void test() {
        Field[] vector = new Field[10];

        for (int i = 0; i < vector.length; i++)
            vector[i] = new Field(FieldStatus.Filled);

        Field[] ref = {
                vector[4],
                vector[5],
                vector[6]
        };

        assertEquals(vector[4].getStatus(), FieldStatus.Filled);
        assertEquals(vector[5].getStatus(), FieldStatus.Filled);
        assertEquals(vector[6].getStatus(), FieldStatus.Filled);

        for (Field f : ref)
            f.setStatus(FieldStatus.Unknown);

        assertEquals(vector[4].getStatus(), FieldStatus.Unknown);
        assertEquals(vector[5].getStatus(), FieldStatus.Unknown);
        assertEquals(vector[6].getStatus(), FieldStatus.Unknown);
    }
}
