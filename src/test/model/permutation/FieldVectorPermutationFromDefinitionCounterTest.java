package test.model.permutation;

import model.definition.FieldVectorDefinition;
import model.permutation.FieldVectorPermutationFromDefinition;
import model.permutation.FieldVectorPermutationFromDefinitionCounter;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class FieldVectorPermutationFromDefinitionCounterTest {
    @Test
    public void permutationCount() throws Exception {
        FieldVectorPermutationFromDefinitionCounter counter = new FieldVectorPermutationFromDefinitionCounter();

        List<Integer> sequenceLengths = Arrays.asList(4, 2);
        int fieldsCount = 8;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(2, 3, 2, 4);
        fieldsCount = 20;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(7);
        fieldsCount = 7;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(7);
        fieldsCount = 8;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(7);
        fieldsCount = 9;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(7);
        fieldsCount = 20;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(1, 4, 4, 1, 1, 1);
        fieldsCount = 25;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(1, 4, 4, 1, 1);
        fieldsCount = 25;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(1, 4, 4, 1, 2, 1);
        fieldsCount = 25;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));

        sequenceLengths = Arrays.asList(1, 1, 1, 1);
        fieldsCount = 25;
        assertTrue(countCompare(sequenceLengths, fieldsCount, counter));
    }

    public boolean countCompare(List<Integer> sequenceLengths, int fieldsCount, FieldVectorPermutationFromDefinitionCounter counter) {
        long byPermutation = counter.permutationCount(sequenceLengths, fieldsCount);
        long byCounting = 0;

        FieldVectorDefinition definition = new FieldVectorDefinition(sequenceLengths);
        FieldVectorPermutationFromDefinition permutation = new FieldVectorPermutationFromDefinition(definition, fieldsCount);

        while (permutation.hasNextPermutation()) {
            permutation.nextPermutation();
            byCounting++;
        }

        System.out.println("By counting:" + byCounting + "\nBy equation: " + byPermutation);

        return byPermutation == byCounting;
    }
}