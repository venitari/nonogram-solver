package test.model.permutation;

import model.Field;
import model.permutation.FieldVectorBinaryPermutation;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class FieldVectorBinaryPermutationTest {
    @Test
    public void test() {
        int length = 4;
        Field[] vector = new Field[4];
        for (int i = 0; i < length; i++)
            vector[i] = new Field();

        FieldVectorBinaryPermutation permutation = new FieldVectorBinaryPermutation(length);

        int permutationCount = 0;
        while (permutation.hasNextPermutation()) {
            permutation.nextPermutation();
            permutation.getFieldVector(vector);
            Field.print(vector);
            permutationCount++;
        }

        assertEquals(permutation.getPermutationCount(), permutationCount);
    }
}