package test.model.permutation;

import model.Field;
import model.FieldStatus;
import model.definition.FieldVectorDefinition;
import model.permutation.FieldVectorPermutationFromDefinition;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class FieldVectorPermutationFromDefinitionTest {
    @Test
    public void nextPermutation() throws Exception {
        FieldVectorDefinition definition = new FieldVectorDefinition(new ArrayList<Integer>());
        FieldVectorPermutationFromDefinition permutation = new FieldVectorPermutationFromDefinition(definition, 14);
        Field[] buffer = new Field[10];

        for (int i = 0; i < buffer.length; i++)
            buffer[i] = new Field();

        buffer[2].setStatus(FieldStatus.Filled);

        while (permutation.hasNextPermutation()) {
            permutation.nextPermutation();
            permutation.getFieldVector(buffer);
            Field.print(buffer);
        }
    }
}