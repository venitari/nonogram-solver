package test.model.definition;

import model.FieldBoard;
import model.FieldStatus;
import model.definition.TableDefinition;
import model.user.input.DefinitionFileLoad;
import model.user.input.FieldBoardLoader;
import model.user.input.NonogramDefinitionInput;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class TableDefinitionTest {

    @Test
    public void matches() throws Exception {
        FieldBoard smiley = FieldBoardLoader.loadFromFile("src/test/files/Smiley.txt");
        NonogramDefinitionInput smileyDefInput = DefinitionFileLoad.loadFromFile(new File("src/test/files/SmileyDefinition.txt"));
        TableDefinition smileyDef = new TableDefinition(smileyDefInput);
        assertTrue(smileyDef.matches(smiley));
    }

    public void printFieldBoard(FieldBoard board) {
        for (int y = 0; y < board.getHeight(); y++) {
            for (int x = 0; x < board.getWidth(); x++)
                System.out.print(board.select(x, y).getStatus() == FieldStatus.Filled ? "1" : "0");

            System.out.println();
        }
    }
}