package test.model.definition;

import model.Field;
import model.FieldStatus;
import model.definition.FieldVectorDefinition;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class FieldVectorDefinitionTest {
    @org.junit.Test
    public void getLengthSequence() throws Exception {
        FieldVectorDefinition definition = new FieldVectorDefinition(testSet1);
        assertEquals(0, definition.getLengthSequence().size());

        definition = new FieldVectorDefinition(testSet2);
        assertEquals(4, definition.getLengthSequence().size());
        assertEquals(3, definition.getLengthSequence().get(0).intValue());
        assertEquals(2, definition.getLengthSequence().get(1).intValue());
        assertEquals(2, definition.getLengthSequence().get(2).intValue());
        assertEquals(1, definition.getLengthSequence().get(3).intValue());

        definition = new FieldVectorDefinition(testSet3);
        assertEquals(5, definition.getLengthSequence().size());
        assertEquals(3, definition.getLengthSequence().get(0).intValue());
        assertEquals(2, definition.getLengthSequence().get(1).intValue());
        assertEquals(2, definition.getLengthSequence().get(2).intValue());
        assertEquals(1, definition.getLengthSequence().get(3).intValue());
        assertEquals(1, definition.getLengthSequence().get(4).intValue());

        definition = new FieldVectorDefinition(testSet4);
        assertEquals(4, definition.getLengthSequence().size());
        assertEquals(3, definition.getLengthSequence().get(0).intValue());
        assertEquals(2, definition.getLengthSequence().get(1).intValue());
        assertEquals(2, definition.getLengthSequence().get(2).intValue());
        assertEquals(1, definition.getLengthSequence().get(3).intValue());
    }

    @org.junit.Test
    public void matches() throws Exception {
        FieldVectorDefinition definition = new FieldVectorDefinition(Arrays.asList(3, 2, 2, 1));
        assertFalse(definition.matches(testSet1));
        assertTrue(definition.matches(testSet2));
        assertFalse(definition.matches(testSet3));
        assertTrue(definition.matches(testSet4));
    }

    @org.junit.Test
    public void getMinimumVectorLength() throws Exception {
        FieldVectorDefinition definition = new FieldVectorDefinition(Arrays.asList(1));
        assertEquals(1, definition.getMinimumVectorLength());
        definition = new FieldVectorDefinition(Arrays.asList(3, 2, 2, 1));
        assertEquals(11, definition.getMinimumVectorLength());
    }

    Field[] testSet1 = new Field[]{};

    Field[] testSet2 = new Field[]{
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled)
    };

    Field[] testSet3 = new Field[]{
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
    };

    Field[] testSet4 = new Field[]{
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Filled),
            new Field(FieldStatus.Empty),
            new Field(FieldStatus.Filled)
    };
}