package view;

import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;

/**
 * Created by Patryk Dziedzic on 22.03.2017.
 */
public class DrawingAreaTab extends Tab {
    public DrawingAreaTab(String caption) {
        super(caption);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setContent(canvas);
        setContent(scrollPane);
        setClosable(false);
    }

    public Canvas getCanvas() {
        return canvas;
    }

    private ScrollPane scrollPane = new ScrollPane();
    private Canvas canvas = new Canvas();
}
