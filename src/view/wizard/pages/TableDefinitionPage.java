package view.wizard.pages;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Patryk Dziedzic on 16.03.2017.
 */
public abstract class TableDefinitionPage extends WizardPage {
    public TableDefinitionPage(int width, int height, String columnLabel) {
        this.columnLabel = columnLabel;
        initList(width, height);
        initColumns(width);
        table.setEditable(true);
        table.setItems(rows);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        scrollPane.setContent(table);

        view = scrollPane;
    }

    @Override
    public Node getView() {
        return view;
    }

    @Override
    public void clear() {
        if (!tableExists())
            return;

        rows.forEach(values -> Arrays.stream(values).forEach(i -> i = 0));
    }

    public List<List<Integer>> getVectorsSequencesLengths() {
        if (!tableExists())
            return new ArrayList<>();

        int width = rows.get(0).length;
        int height = rows.size();

        List<Integer> sequencesLengths;
        List<List<Integer>> vectorSequences = new ArrayList<List<Integer>>(width);

        for (int i = 0; i < width; i++) {
            sequencesLengths = new ArrayList<>(height);

            for (int j = 0; j < height; j++) {
                Integer value = rows.get(j)[i];
                if (value > 0)
                    sequencesLengths.add(value);
            }

            vectorSequences.add(sequencesLengths);
        }

        return vectorSequences;
    }

    public void fillTable(List<List<Integer>> vectorSequencesLengths) {
        if (vectorSequencesLengths == null ||
                vectorSequencesLengths.size() == 0 ||
                !tableExists())
            return;

        int width = rows.get(0).length;
        int height = rows.size();

        for (int i = 0; i < width; i++) {
            if (vectorSequencesLengths.get(i) == null ||
                    vectorSequencesLengths.get(i).size() == 0)
                continue;

            for (int j = 0; j < height; j++)
                if (vectorSequencesLengths.size() > i && vectorSequencesLengths.get(i).size() > j)
                    rows.get(j)[i] = vectorSequencesLengths.get(i).get(j);
                else
                    rows.get(j)[i] = 0;
        }
    }

    private boolean tableExists() {
        return !(rows.size() == 0 ||
                rows.get(0) == null ||
                rows.get(0).length == 0);
    }

    private void initList(int width, int height) {
        for (int i = 0; i < height; i++) {
            int[] row = new int[width];

            for (int j = 0; j < width; j++)
                row[i] = 0;

            rows.add(row);
        }
    }

    private void initColumns(int width) {
        for (int i = 0; i < width; i++) {
            final int index = i;
            TableColumn<int[], String> col = new TableColumn<>(columnLabel + " " + (i + 1));
            col.setCellFactory(TextFieldTableCell.forTableColumn());
            col.setCellValueFactory(dataCell -> new SimpleStringProperty(Integer.toString(dataCell.getValue()[index])));
            setHandler(col);
            table.getColumns().add(col);
        }
    }

    private void setHandler(TableColumn<int[], String> col) {
        col.setOnEditCommit(cell -> {
            int column = cell.getTablePosition().getColumn();
            cell.getRowValue()[column] = Integer.parseInt(cell.getNewValue());
        });
    }

    private Node view;
    private String columnLabel = "col";
    private TableView<int[]> table = new TableView<>();
    private ObservableList<int[]> rows = FXCollections.observableArrayList();
}
