package view.wizard.pages;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Created by Patryk Dziedzic on 16.03.2017.
 */
public class SizePage extends WizardPage {
    public SizePage() {
        VBox mainPane = new VBox();
        HBox widthRow = new HBox();
        HBox heightRow = new HBox();
        mainPane.setPadding(new Insets(10, 10, 10, 10));
        mainPane.setSpacing(10);
        mainPane.setAlignment(Pos.CENTER);
        widthRow.setSpacing(10);
        widthRow.setAlignment(Pos.CENTER);
        widthRow.getChildren().addAll(widthLabel, widthTextField);
        heightRow.setSpacing(10);
        heightRow.setAlignment(Pos.CENTER);
        heightRow.getChildren().addAll(heightLabel, heightTextField);

        mainPane.getChildren().addAll(widthRow, heightRow);
        view = mainPane;
    }

    public String getWidthText() {
        return widthTextField.getText();
    }

    public String getHeightText() {
        return heightTextField.getText();
    }

    public void setWidthText(String text) {
        widthTextField.setText(text);
    }

    public void setHeightText(String text) {
        heightTextField.setText(text);
    }

    @Override
    public Node getView() {
        return view;
    }

    @Override
    public void clear() {
        widthTextField.setText("");
        heightTextField.setText("");
    }

    private Node view;
    private TextField widthTextField = new TextField();
    private TextField heightTextField = new TextField();
    private Label widthLabel = new Label("Width:");
    private Label heightLabel = new Label("Height:");
}
