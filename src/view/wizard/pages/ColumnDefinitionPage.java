package view.wizard.pages;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class ColumnDefinitionPage extends TableDefinitionPage {
    public ColumnDefinitionPage(int columnCount) {
        super(columnCount, columnCount / 2 + 1, "Col");
    }
}
