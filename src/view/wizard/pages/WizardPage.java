package view.wizard.pages;

import javafx.scene.Node;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public abstract class WizardPage {
    public abstract void clear();

    public abstract Node getView();
}
