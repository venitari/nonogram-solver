package view.wizard.pages;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class RowDefinitionPage extends TableDefinitionPage {
    public RowDefinitionPage(int rowCount) {
        super(rowCount, rowCount / 2 + 1, "Row ");
    }
}
