package model;

/**
 * Created by Patryk Dziedzic on 14.03.2017.
 */
public class FieldBoard {
    public FieldBoard(int width, int height) {
        fields = new Field[height][];

        for (int i = 0; i < height; i++) {
            fields[i] = new Field[width];
            initRow(fields[i]);
        }
    }

    public static void setAsUnknown(FieldBoard board) {
        Field.setAsUnknown(board.fields);
    }

    public Field[] selectColumn(int col) {
        Field[] column = new Field[getHeight()];

        for (int i = 0; i < getHeight(); i++)
            column[i] = getField(col, i);

        return column;
    }

    public Field[] selectRow(int row) {
        return fields[row];
    }

    public Field select(int x, int y) {
        return fields[y][x];
    }

    public int getWidth() {
        if (!hasFields())
            return 0;

        return fields[0].length;
    }

    public int getHeight() {
        if (!hasFields())
            return 0;

        return fields.length;
    }

    private Field getField(int x, int y) {
        return fields[y][x];
    }

    private boolean hasFields() {
        return fields != null &&
                fields.length > 0 ||
                fields[0] != null ||
                fields[0].length > 0;
    }

    private void initRow(Field[] row) {
        for (int i = 0; i < row.length; i++)
            row[i] = new Field();
    }

    private Field[][] fields;
}
