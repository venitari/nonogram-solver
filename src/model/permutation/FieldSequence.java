package model.permutation;

import model.Field;
import model.FieldStatus;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class FieldSequence extends Field {
    public FieldSequence(FieldStatus status, int length) {
        this.length = length;
        setStatus(status);
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    private int length;
}
