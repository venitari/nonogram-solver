package model.permutation;

import model.Field;
import model.FieldStatus;
import model.definition.FieldVectorDefinition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
class SequenceList {
    public Field[] getFieldVector(Field[] vectorToFill) {
        if (vectorToFill == null)
            return null;

        int fieldIndex = 0;

        for (int i = 0; i < sequenceList.size(); i++) {
            FieldStatus status = sequenceList.get(i).getStatus();

            for (int j = 0; j < sequenceList.get(i).getLength(); j++) {
                if (fieldIndex >= vectorToFill.length)
                    return vectorToFill;

                vectorToFill[fieldIndex].setStatus(status);
                fieldIndex++;
            }
        }

        for (int i = fieldIndex; i < vectorToFill.length && i < fieldsCount; i++)
            vectorToFill[i].setStatus(FieldStatus.Empty);

        return vectorToFill;
    }

    public Field[] getFieldVector() {
        Field[] fields = new Field[fieldsCount];

        for (int i = 0; i < fields.length; i++)
            fields[i] = new Field();

        return getFieldVector(fields);
    }

    protected SequenceList(FieldVectorDefinition definition, int fieldsCount) {
        this.definition = definition;
        this.fieldsCount = fieldsCount;
    }

    protected void squeezeNodes(int initNode) {
        int spaceReduced = 0;

        for (int i = initNode; i < sequenceList.size(); i++) {
            FieldSequence node = sequenceList.get(i);

            if (!node.isEmpty())
                continue;

            spaceReduced += (node.getLength() - 1);
            node.setLength(1);
        }

        if (spaceReduced == 0)
            return;

        FieldSequence lastNode = sequenceList.get(sequenceList.size() - 1);

        if (lastNode.isEmpty())
            lastNode.setLength(lastNode.getLength() + spaceReduced);
        else
            sequenceList.add(new FieldSequence(FieldStatus.Empty, spaceReduced));
    }

    protected int getPreviousFilledFieldIndex(int initNodeIndex) {
        for (int i = initNodeIndex - 1; i >= 0; i--)
            if (sequenceList.get(i).isFilled())
                return i;

        return -1;
    }

    protected int size() {
        return sequenceList.size();
    }

    protected void createInitSequenceTree() {
        sequenceList.clear();

        if (definition.getLengthSequence().size() == 0)
            return;

        sequenceList.add(new FieldSequence(FieldStatus.Filled, definition.getLengthSequence().get(0)));

        for (int i = 1; i < definition.getLengthSequence().size(); i++) {
            sequenceList.add(new FieldSequence(FieldStatus.Empty, MIN_DISTANCE));
            sequenceList.add(new FieldSequence(FieldStatus.Filled, definition.getLengthSequence().get(i)));
        }

        completeWithEmptySequence();
    }

    private void completeWithEmptySequence() {
        int currentCount = sequenceList.stream().mapToInt(FieldSequence::getLength).sum();

        if (fieldsCount <= currentCount)
            return;

        int missingFields = fieldsCount - currentCount;
        FieldSequence lastSequence = sequenceList.get(sequenceList.size() - 1);

        if (lastSequence.isEmpty())
            lastSequence.setLength(lastSequence.getLength() + missingFields);
        else
            sequenceList.add(new FieldSequence(FieldStatus.Empty, missingFields));
    }

    static final int MIN_DISTANCE = 1;
    protected List<FieldSequence> sequenceList = new ArrayList<>();
    private int fieldsCount;
    private FieldVectorDefinition definition;
}
