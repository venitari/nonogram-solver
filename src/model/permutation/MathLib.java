package model.permutation;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class MathLib {
    public long factorial(long n) {
        if (n < 0)
            return 0;

        long res = 1;
        for (long i = 2; i <= n; i++)
            res *= i;

        return res;
    }

    public long newton(long k, long n) {
        if (k < 0 || n < 0)
            return 0;
        if (k == 0 || n == 0)
            return 1;

        long start = n - k + 1;
        long res = 1;

        for (long i = start; i <= n; i++)
            res *= i;

        return res / factorial(k);
    }
}
