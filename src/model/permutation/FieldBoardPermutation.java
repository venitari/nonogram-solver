package model.permutation;

import controller.CanvasNonogramPrinter;
import model.Field;
import model.FieldBoard;
import model.definition.FieldVectorDefinition;
import model.definition.TableDefinition;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class FieldBoardPermutation {
    public FieldBoardPermutation(TableDefinition definition) {
        board = new FieldBoard(definition.getWidth(), definition.getHeight());
        this.definition = definition;
    }

    public FieldBoard getCurrentBoard() {
        return board;
    }

    public void reset() {
        prepareForReset();

        int currentPermutator = 0;
        boolean found = false;

        while (!found && currentPermutator >= 0) {
            if (currentPermutator >= rowPermutators.size()) {
                if (definition.matches(board))
                    found = true;
                else
                    currentPermutator--;
            } else {
                RowPermutator permutation = rowPermutators.get(currentPermutator);

                if (permutation.getRowPermutator().hasNextPermutation()) {
                    permutation.getRowPermutator().nextPermutation();
                    assignPermutationToBoard(permutation);

                    if (definition.columnsPossibleToMatchDefinition(board))
                        currentPermutator++;
                } else {
                    Field.setAsUnknown(board.selectRow(permutation.getRowIndex()));
                    permutation.getRowPermutator().reset();
                    currentPermutator--;
                }
            }
        }
    }

    public void resetWithAnimation(CanvasNonogramPrinter printer, int intervalMilis) {
        prepareForReset();
        int currentPermutator = 0;
        boolean found = false;

        while (!found && currentPermutator >= 0) {
            if (currentPermutator >= rowPermutators.size()) {
                if (definition.matches(board)) {
                    System.out.println("Zrobiłem permutacje dla wszystkich wierszy i pasuje!");
                    found = true;
                } else {
                    System.out.println("Zrobiłem permutacje dla wszystkich wierszy i nie pasuje :(");
                    currentPermutator--;
                }
            } else {
                RowPermutator permutation = rowPermutators.get(currentPermutator);

                if (permutation.getRowPermutator().hasNextPermutation()) {
                    permutation.getRowPermutator().nextPermutation();
                    assignPermutationToBoard(permutation);

                    if (definition.columnsPossibleToMatchDefinition(board))
                        currentPermutator++;
                } else {
                    Field.setAsUnknown(board.selectRow(permutation.getRowIndex()));
                    permutation.getRowPermutator().reset();
                    currentPermutator--;
                }
            }

            try {
                printer.printNonogram(board);
                Thread.sleep(intervalMilis);
            } catch (Exception e) {
            }
        }

        System.out.println("Dotarłem do indeksu: " + currentPermutator);
        System.out.println("Znalazłem? " + found);
    }

    private void prepareForReset() {
        FieldBoard.setAsUnknown(board);
        initAndSortRowPermutators(definition);
        fillRowsWithDeterminedFieldsForSortedPermutators();
    }

    private void fillRowsWithDeterminedFieldsForSortedPermutators() {
        if (rowPermutators == null && rowPermutators.size() == 0)
            return;

        while (rowPermutators.size() > 0 && rowPermutators.get(0).getPermutationCount() == 1) {
            rowPermutators.get(0).getRowPermutator().nextPermutation();
            assignPermutationToBoard(rowPermutators.get(0));
            rowPermutators.remove(0);
        }
    }

    private void assignPermutationToBoard(RowPermutator permutator) {
        permutator.getRowPermutator().getFieldVector(board.selectRow(permutator.getRowIndex()));
    }

    private void initAndSortRowPermutators(TableDefinition definition) {
        rowPermutators = new ArrayList<>(definition.getHeight());

        for (int i = 0; i < definition.getHeight(); i++)
            rowPermutators.add(new RowPermutator(definition.getWidth(), i, definition.getRowsDefinitions().get(i)));

        rowPermutators.sort(Comparator.comparingLong(RowPermutator::getPermutationCount));
    }

    private FieldBoard board;
    private TableDefinition definition;
    private List<RowPermutator> rowPermutators;
}

class RowPermutator {
    public int getRowIndex() {
        return rowIndex;
    }

    public FieldVectorPermutationFromDefinition getRowPermutator() {
        return permutator;
    }

    public RowPermutator(int rowLength, int rowIndex, FieldVectorDefinition rowDefinition) {
        permutator = new FieldVectorPermutationFromDefinition(rowDefinition, rowLength);
        this.rowIndex = rowIndex;
    }

    public long getPermutationCount() {
        return permutator.getPermutationCount();
    }

    private int rowIndex;
    private FieldVectorPermutationFromDefinition permutator;
}