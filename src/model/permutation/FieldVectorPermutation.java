package model.permutation;

import model.Field;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public interface FieldVectorPermutation {
    boolean hasNextPermutation();

    void nextPermutation();

    long getPermutationCount();

    Field[] getFieldVector(Field[] vectorToFill);
}
