package model.permutation;

import model.Field;
import model.FieldStatus;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class FieldVectorBinaryPermutation implements FieldVectorPermutation {
    public FieldVectorBinaryPermutation(int length) {
        vector = new Field[length];

        for (int i = 0; i < vector.length; i++)
            vector[i] = new Field();

        reset = true;
    }

    @Override
    public boolean hasNextPermutation() {
        if (reset)
            return true;

        for (Field f : vector)
            if (f.isEmpty())
                return true;

        return false;
    }

    @Override
    public void nextPermutation() {
        if (reset) {
            reset();
            return;
        }

        int current = 0;

        while (current >= 0 && vector[current].isFilled()) {
            vector[current].setStatus(FieldStatus.Empty);
            current++;
        }

        if (current < vector.length)
            vector[current].setStatus(FieldStatus.Filled);
    }

    @Override
    public long getPermutationCount() {
        long result = 1;

        for (Field f : vector)
            result *= 2;

        return result;
    }

    @Override
    public Field[] getFieldVector(Field[] vectorToFill) {
        for (int i = 0; i < vectorToFill.length && i < vector.length; i++)
            vectorToFill[i].setStatus(vector[i].getStatus());

        return vectorToFill;
    }

    private void reset() {
        reset = false;

        for (Field f : vector)
            f.setStatus(FieldStatus.Empty);
    }

    private boolean reset;
    private Field[] vector;
}
