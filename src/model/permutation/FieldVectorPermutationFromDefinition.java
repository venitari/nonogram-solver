package model.permutation;

import model.FieldStatus;
import model.definition.FieldVectorDefinition;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class FieldVectorPermutationFromDefinition extends SequenceList implements FieldVectorPermutation {
    public FieldVectorPermutationFromDefinition(FieldVectorDefinition definition, int fieldsCount) {
        super(definition, fieldsCount);
        permutationCount = new FieldVectorPermutationFromDefinitionCounter().permutationCount(definition.getLengthSequence(), fieldsCount);
        reset();
    }

    public boolean hasNextPermutation() {
        return reseted && permutationCount > 0 ||
                getLastMovableNodeIndex() >= 0;

    }

    public long getPermutationCount() {
        return permutationCount;
    }

    public void nextPermutation() {
        if (reseted) {
            createInitSequenceTree();
            reseted = false;
            return;
        }

        int index = getLastMovableNodeIndex();

        if (index < 0)
            return;

        moveNode(index);
        squeezeNodes(index);
    }

    public void reset() {
        reseted = true;
    }

    private void moveNode(int nodeIndex) {
        int reduction = countEmptySpaceAfterToReduce(nodeIndex);
        reduceEmptySpaceAfter(nodeIndex, reduction);
        enlargeEmptySpaceBefore(nodeIndex, reduction);
    }

    private int countEmptySpaceAfterToReduce(int nodeIndex) {
        if (nodeIndex >= size() - 1)
            return 0;

        FieldSequence next = sequenceList.get(nodeIndex + 1);

        if (!next.isEmpty())
            return 0;

        int difference = next.getLength() - MIN_DISTANCE;

        if (difference <= 0) {
            if (nodeIndex + 1 == size() - 1)
                return next.getLength();
            else
                return 0;
        }

        return difference >= MIN_DISTANCE ? MIN_DISTANCE : difference;
    }

    private void reduceEmptySpaceAfter(int nodeIndex, int reduceCount) {
        if (nodeIndex >= size() - 1)
            return;

        FieldSequence next = sequenceList.get(nodeIndex + 1);
        if (next.getLength() == reduceCount)
            sequenceList.remove(nodeIndex + 1);
        else
            next.setLength(next.getLength() - reduceCount);
    }

    private void enlargeEmptySpaceBefore(int nodeIndex, int size) {
        if (nodeIndex == 0) {
            sequenceList.add(0, new FieldSequence(FieldStatus.Empty, size));
            return;
        }

        FieldSequence prev = sequenceList.get(nodeIndex - 1);
        prev.setLength(prev.getLength() + size);
    }

    private int getLastMovableNodeIndex() {
        int nodeIndex = size() - 1;

        while (nodeIndex >= 0 && !nodeHasSpaceToMove(nodeIndex))
            nodeIndex = getPreviousFilledFieldIndex(nodeIndex);

        return nodeIndex;
    }

    private boolean nodeHasSpaceToMove(int initNodeIndex) {
        if (initNodeIndex >= size() - 1)
            return false;

        FieldSequence next = sequenceList.get(initNodeIndex + 1);

        if (next.isEmpty()) {
            if (next.getLength() > MIN_DISTANCE)
                return true;
            else if (next.getLength() > 0 && initNodeIndex + 1 == size() - 1)
                return true;
        }

        return false;
    }

    private long permutationCount;
    private boolean reseted = false;
}
