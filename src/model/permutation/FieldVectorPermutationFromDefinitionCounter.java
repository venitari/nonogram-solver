package model.permutation;

import java.util.List;

/**
 * Created by Patryk Dziedzic on 15.03.2017.
 */
public class FieldVectorPermutationFromDefinitionCounter {
    private MathLib math = new MathLib();

    public long permutationCount(List<Integer> sequenceLengths, int fieldsCount) {
        int filledFieldsCount = sequenceLengths.stream().mapToInt(Integer::intValue).sum();
        int sequencesCount = sequenceLengths.size();
        return permutationCount(fieldsCount, filledFieldsCount, sequencesCount);
    }

    private long permutationCount(int fieldsCount, int filledFieldsCount, int sequencesCount) {
        long minLengthForSequences = filledFieldsCount + sequencesCount - 1;

        if (fieldsCount < minLengthForSequences)
            return 0;
        else if (sequencesCount == 1) {
            return 1 + fieldsCount - filledFieldsCount;
        }

        return math.newton(sequencesCount - 2, fieldsCount - filledFieldsCount - 1) +
                2 * math.newton(sequencesCount - 1, fieldsCount - filledFieldsCount - 1) +
                math.newton(sequencesCount, fieldsCount - filledFieldsCount - 1);
    }
}
