package model.user.input;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created by Patryk Dziedzic on 19.03.2017.
 */
public class DefinitionFileLoad {
    private static final Pattern numSeparator = Pattern.compile("[ ]+");

    private DefinitionFileLoad() {
    }

    public static NonogramDefinitionInput loadFromFile(File file) throws Exception {
        Scanner sc = new Scanner(file);
        NonogramDefinitionInput definition = new NonogramDefinitionInput();
        DefinitionFileLoad loader = new DefinitionFileLoad();

        while (sc.hasNextLine()) {
            String header = sc.nextLine();
            loader.headerAction(header, definition, sc);
        }

        sc.close();
        return definition;
    }

    private NonogramDefinitionInput headerAction(String header, NonogramDefinitionInput input, Scanner sc) throws Exception {
        if (header.equals("SIZE")) {
            List<Integer> dimensions = readLengthsSequence(sc);
            input.setWidth(dimensions.get(0));
            input.setHeight(dimensions.get(1));
        } else if (header.equals("COLUMNS")) {
            List<List<Integer>> columns = readVectorLengthsSequences(sc, input.getWidth());
            input.setColumnsSequencesLengths(columns);
        } else if (header.equals("ROWS")) {
            List<List<Integer>> rows = readVectorLengthsSequences(sc, input.getHeight());
            input.setRowsSequencesLengths(rows);
        } else
            throw new Exception("Header unknown: " + header + "\n");

        return input;
    }

    private List<List<Integer>> readVectorLengthsSequences(Scanner sc, int vectorsCount) {
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < vectorsCount; i++)
            result.add(readLengthsSequence(sc));

        return result;
    }

    private List<Integer> readLengthsSequence(Scanner sc) {
        List<Integer> result = new ArrayList<>();

        String input;

        if (!sc.hasNextLine())
            return result;

        input = sc.nextLine();

        Scanner lineScanner = new Scanner(input);
        lineScanner.useDelimiter(numSeparator);

        while (lineScanner.hasNextInt())
            result.add(lineScanner.nextInt());

        lineScanner.close();
        return result;
    }
}
