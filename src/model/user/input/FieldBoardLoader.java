package model.user.input;

import model.FieldBoard;
import model.FieldStatus;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class FieldBoardLoader {
    public static FieldBoard loadFromFile(String filepath) {
        FieldBoard board = null;

        try {
            FieldBoardLoader loader = new FieldBoardLoader();
            board = loader.getBoardFromFile(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return board;
    }

    private FieldBoard getBoardFromFile(String filepath) throws Exception {
        List<String> content = Files.readAllLines(Paths.get(filepath));

        if (content == null || content.size() == 0)
            throw new Exception("Empty file");

        FieldBoard board = createEmptyBoard(content);
        fillBoard(board, content);

        return board;
    }

    private FieldBoard fillBoard(FieldBoard board, List<String> fileContent) throws Exception {
        for (int i = 0; i < board.getHeight(); i++) {
            FieldStatus[] vector = readFromString(fileContent.get(i));

            if (vector.length != board.getWidth())
                throw new Exception("Row no. " + i + " has different length(" + vector.length + ") than board width(" + board.getWidth() + ")");

            writeRow(board, vector, i);
        }

        return board;
    }

    private FieldBoard createEmptyBoard(List<String> fileContent) throws Exception {
        int width = getWidth(fileContent);
        int height = fileContent.size();
        return new FieldBoard(width, height);
    }

    private int getWidth(List<String> fileContent) throws Exception {
        FieldStatus[] vector = readFromString(fileContent.get(0));
        return vector.length;
    }

    private FieldBoard writeRow(FieldBoard board, FieldStatus[] vector, int row) throws Exception {
        for (int i = 0; i < vector.length; i++)
            board.select(i, row).setStatus(vector[i]);

        return board;
    }

    private FieldStatus[] readFromString(String line) throws Exception {
        Scanner sc = new Scanner(line);
        List<Integer> values = new ArrayList<>();

        while (sc.hasNextInt())
            values.add(sc.nextInt());

        return values.stream().map(i -> i == 1 ? FieldStatus.Filled : FieldStatus.Empty).toArray(FieldStatus[]::new);
    }
}
