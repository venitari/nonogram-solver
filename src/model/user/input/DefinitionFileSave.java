package model.user.input;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Patryk Dziedzic on 20.03.2017.
 */
public class DefinitionFileSave {
    private DefinitionFileSave() {
    }

    public static void saveToFile(File f, NonogramDefinitionInput input) throws Exception {
        DefinitionFileSave save = new DefinitionFileSave();
        StringBuilder builder = new StringBuilder();
        builder = save.writeSize(builder, input);
        builder = save.writeColumns(builder, input);
        builder = save.writeRows(builder, input);

        PrintWriter pw = new PrintWriter(f);
        pw.print(builder);
        pw.close();
    }

    private StringBuilder writeRows(StringBuilder builder, NonogramDefinitionInput input) {
        builder.append("ROWS\n");
        return appendSequencesLengths(builder, input.getRowsSequencesLengths());
    }

    private StringBuilder writeColumns(StringBuilder builder, NonogramDefinitionInput input) {
        builder.append("COLUMNS\n");
        return appendSequencesLengths(builder, input.getColumnsSequencesLengths());
    }

    private StringBuilder writeSize(StringBuilder builder, NonogramDefinitionInput input) {
        builder.append("SIZE\n");
        return builder.append(input.getWidth()).append(" ").append(input.getHeight()).append("\n");
    }

    private StringBuilder appendSequencesLengths(StringBuilder builder, List<List<Integer>> sequencesLengths) {
        for (List<Integer> sequenceLengths : sequencesLengths)
            appendSequenceLengths(builder, sequenceLengths);
        return builder;
    }

    private StringBuilder appendSequenceLengths(StringBuilder builder, List<Integer> sequenceLength) {
        return builder.append(sequenceLength.stream().
                map(Object::toString).
                collect(Collectors.joining(" "))).
                append("\n");
    }
}
