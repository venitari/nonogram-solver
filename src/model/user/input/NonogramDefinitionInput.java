package model.user.input;

import java.util.List;

/**
 * Created by Patryk Dziedzic on 18.03.2017.
 */
public class NonogramDefinitionInput {
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<List<Integer>> getColumnsSequencesLengths() {
        return columnsSequencesLengths;
    }

    public void setColumnsSequencesLengths(List<List<Integer>> columnsSequencesLengths) {
        this.columnsSequencesLengths = columnsSequencesLengths;
    }

    public List<List<Integer>> getRowsSequencesLengths() {
        return rowsSequencesLengths;
    }

    public void setRowsSequencesLengths(List<List<Integer>> rowsSequencesLengths) {
        this.rowsSequencesLengths = rowsSequencesLengths;
    }

    private int width;
    private int height;
    private List<List<Integer>> columnsSequencesLengths;
    private List<List<Integer>> rowsSequencesLengths;
}
