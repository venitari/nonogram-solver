package model;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class Field {
    public Field() {
    }

    public Field(FieldStatus status) {
        setStatus(status);
    }

    public static void setAsUnknown(Field[] vector) {
        if (vector == null)
            return;

        for (Field field : vector)
            field.setStatus(FieldStatus.Unknown);
    }

    public static void setAsUnknown(Field[][] table) {
        if (table == null)
            return;

        for (Field[] vector : table)
            setAsUnknown(vector);
    }

    public static void print(Field[] vector) {
        for (Field f : vector)
            System.out.print(f.getStatus());

        System.out.println();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Field))
            return false;

        Field f = (Field) other;
        return f.status.equals(status);
    }

    public FieldStatus getStatus() {
        return status;
    }

    public void setStatus(FieldStatus status) {
        this.status = status;
    }

    public boolean isEmpty() {
        return status == FieldStatus.Empty;
    }

    public boolean isFilled() {
        return status == FieldStatus.Filled;
    }

    public boolean isUnknown() {
        return status == FieldStatus.Unknown;
    }

    private FieldStatus status = FieldStatus.Empty;
}
