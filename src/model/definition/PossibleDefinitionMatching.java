package model.definition;

import model.Field;
import model.permutation.FieldVectorBinaryPermutation;
import model.permutation.FieldVectorPermutationFromDefinition;

import java.util.Arrays;

/**
 * Created by Patryk Dziedzic on 26.03.2017.
 */
public class PossibleDefinitionMatching {
    public boolean possibleMatch(FieldVectorDefinition definition, Field[] vector) {
        if (definition.matches(vector))
            return true;
        else if (Arrays.stream(vector).filter(Field::isFilled).count() > definition.getFilledFieldsCount() ||
                Arrays.stream(vector).filter(Field::isEmpty).count() > vector.length - definition.getFilledFieldsCount())
            return false;

        int unknownCount = (int) Arrays.stream(vector).filter(Field::isUnknown).count();

        if (unknownCount == 0)
            return false;

        PermutatorType type = getComparingFunction(definition, vector);

        if (type.equals(PermutatorType.Binary))
            return checkByFillingUnkownFields(definition, vector);
        else
            return checkByComparingVectors(definition, vector);
    }

    private boolean checkByFillingUnkownFields(FieldVectorDefinition definition, Field[] vector) {
        Field[] unknown = Arrays.stream(vector).filter(Field::isUnknown).toArray(Field[]::new);

        FieldVectorBinaryPermutation permutation = new FieldVectorBinaryPermutation(unknown.length);
        while (permutation.hasNextPermutation()) {
            permutation.nextPermutation();
            permutation.getFieldVector(unknown);

            if (definition.matches(vector)) {
                Field.setAsUnknown(unknown);
                return true;
            }
        }

        Field.setAsUnknown(unknown);
        return false;
    }

    private boolean checkByComparingVectors(FieldVectorDefinition definition, Field[] vector) {
        Field[] model = new Field[vector.length];

        for (int i = 0; i < model.length; i++)
            model[i] = new Field();

        FieldVectorPermutationFromDefinition permutation = new FieldVectorPermutationFromDefinition(definition, vector.length);

        while (permutation.hasNextPermutation()) {
            permutation.nextPermutation();
            permutation.getFieldVector(model);

            if (vectorMatchesModel(model, vector))
                return true;
        }

        return false;
    }

    private boolean vectorMatchesModel(Field[] model, Field[] vector) {
        for (int i = 0; i < model.length; i++) {
            if (vector[i].isUnknown())
                continue;

            if (!vector[i].getStatus().equals(model[i].getStatus()))
                return false;
        }

        return true;
    }

    private PermutatorType getComparingFunction(FieldVectorDefinition definition, Field[] vector) {
        FieldVectorPermutationFromDefinition permutator = new FieldVectorPermutationFromDefinition(definition, vector.length);
        int unknownCount = (int) Arrays.stream(vector).filter(Field::isUnknown).count();
        FieldVectorBinaryPermutation binPermutator = new FieldVectorBinaryPermutation(unknownCount);

        if (binPermutator.getPermutationCount() > permutator.getPermutationCount())
            return PermutatorType.Definition;
        else
            return PermutatorType.Binary;
    }

    enum PermutatorType {
        Binary,
        Definition
    }
}
