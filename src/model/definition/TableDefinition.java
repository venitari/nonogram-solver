package model.definition;
import model.Field;
import model.FieldBoard;
import model.user.input.NonogramDefinitionInput;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk Dziedzic on 14.03.2017.
 */
public class TableDefinition {
    public TableDefinition() {
    }

    public TableDefinition(FieldBoard board) {
        initColumnDefinitions(board);
        initRowDefinitions(board);
    }

    public TableDefinition(NonogramDefinitionInput input) {
        for (List<Integer> column : input.getColumnsSequencesLengths())
            colDefinition.add(new FieldVectorDefinition(column));

        for (List<Integer> row : input.getRowsSequencesLengths())
            rowDefinition.add(new FieldVectorDefinition(row));
    }

    public boolean columnsPossibleToMatchDefinition(FieldBoard board) {
        for (int i = 0; i < board.getWidth(); i++)
            if (!matchingCheck.possibleMatch(colDefinition.get(i), board.selectColumn(i)))
                return false;

        return true;
    }

    public int getWidth() {
        return colDefinition.size();
    }

    public int getHeight() {
        return rowDefinition.size();
    }

    public List<FieldVectorDefinition> getRowsDefinitions() {
        return rowDefinition;
    }

    public List<FieldVectorDefinition> getColumnsDefinitions() {
        return colDefinition;
    }

    public boolean matches(FieldBoard board) {
        return columnsMatch(board) &&
                rowsMatch(board);
    }

    public boolean columnsMatch(FieldBoard board) {
        for (int i = 0; i < colDefinition.size() && i < board.getWidth(); i++)
            if (!colDefinition.get(i).matches(board.selectColumn(i)))
                return false;

        return true;
    }

    public boolean rowsMatch(FieldBoard board) {
        for (int i = 0; i < rowDefinition.size() && i < board.getHeight(); i++)
            if (!rowDefinition.get(i).matches(board.selectRow(i)))
                return false;

        return true;
    }

    public void addColumnDefinition(List<Integer> colSequenceLength) {
        colDefinition.add(new FieldVectorDefinition(colSequenceLength));
    }

    public void addColumnDefinition(Field[] colVector) {
        colDefinition.add(new FieldVectorDefinition(colVector));
    }

    public void addRowDefinition(List<Integer> rowSequenceLength) {
        rowDefinition.add(new FieldVectorDefinition(rowSequenceLength));
    }

    public void addRowDefinition(Field[] rowVector) {
        rowDefinition.add(new FieldVectorDefinition(rowVector));
    }

    private void initColumnDefinitions(FieldBoard board) {
        for (int i = 0; i < board.getWidth(); i++)
            addColumnDefinition(board.selectColumn(i));
    }

    private void initRowDefinitions(FieldBoard board) {
        for (int i = 0; i < board.getHeight(); i++)
            addRowDefinition(board.selectRow(i));
    }

    private PossibleDefinitionMatching matchingCheck = new PossibleDefinitionMatching();
    private List<FieldVectorDefinition> rowDefinition = new ArrayList<>();
    private List<FieldVectorDefinition> colDefinition = new ArrayList<>();
}
