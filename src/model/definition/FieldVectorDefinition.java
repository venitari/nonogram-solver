package model.definition;

import model.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public class FieldVectorDefinition {
    public FieldVectorDefinition() {
    }

    public FieldVectorDefinition(Field[] vector) {
        defineFromExistingVector(vector);
    }

    public FieldVectorDefinition(List<Integer> initLengthSequence) {
        lengthSequence = initLengthSequence;
    }

    public List<Integer> getLengthSequence() {
        return lengthSequence;
    }

    public void setLengthSequence(List<Integer> lengthSequence) {
        this.lengthSequence = lengthSequence;
    }

    public boolean matches(Field[] vector) {
        FieldVectorDefinition definition = new FieldVectorDefinition(vector);
        return equals(definition);
    }

    public int getFilledFieldsCount() {
        return lengthSequence.stream().mapToInt(Integer::valueOf).sum();
    }

    public int getMinimumVectorLength() {
        int sum = lengthSequence.stream().mapToInt(Integer::intValue).sum();

        if (lengthSequence.size() > 1)
            sum += (lengthSequence.size() - 1);

        return sum;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof FieldVectorDefinition))
            return false;

        FieldVectorDefinition definition = (FieldVectorDefinition) other;

        if (lengthSequence.size() != definition.lengthSequence.size())
            return false;

        for (int i = 0; i < lengthSequence.size(); i++)
            if (!lengthSequence.get(i).equals(definition.lengthSequence.get(i)))
                return false;

        return true;
    }

    private void defineFromExistingVector(Field[] vector) {
        lengthSequence.clear();

        if (vector == null)
            return;

        int currentSeqLength = 0;

        for (int i = 0; i < vector.length; i++) {
            if (vector[i].isFilled())
                currentSeqLength++;
            else if (currentSeqLength > 0) {
                lengthSequence.add(currentSeqLength);
                currentSeqLength = 0;
            }
        }

        if (currentSeqLength > 0)
            lengthSequence.add(currentSeqLength);
    }

    private List<Integer> lengthSequence = new ArrayList<>();
}
