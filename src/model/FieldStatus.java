package model;

/**
 * Created by Patryk Dziedzic on 12.03.2017.
 */
public enum FieldStatus {
    Empty,
    Filled,
    Unknown
}
